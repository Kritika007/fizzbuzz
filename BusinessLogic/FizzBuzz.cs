﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class FizzBuzz : IFizzBuzz
    {
        private IDFizzBuzz dFizzBuzz;
        //private readonly DateTime _now = DateTime.UtcNow;

        public List<string> GetFizzBuzz(int val)
        {
            var vals = new List<string>();
            for (var i = 1; i <= val; i++)
            {
                if (i % 3 == 0 && i % 5 == 0)
                {
                    dFizzBuzz = new FizzBuzz1();
                }

                else if (i % 3 == 0)
                {
                    dFizzBuzz = new FizzBuzz2();
                }
                else if (i % 5 == 0)
                {
                    dFizzBuzz = new FizzBuzz3();
                }

                vals.Add(dFizzBuzz.GetFizzBuzzValue(i));
            }
            return vals;
        }

    }
}
