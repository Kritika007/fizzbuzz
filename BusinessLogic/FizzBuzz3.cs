﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class FizzBuzz3 : IDFizzBuzz
    {
        private readonly DateTime _now = DateTime.UtcNow;
        public string GetFizzBuzzValue(int i)
        {
            if (i % 5 == 0)
            {
                if (_now.DayOfWeek == DayOfWeek.Wednesday)
                    return "Wuzz";
                else
                    return " Buzz";
            }
            else
            {
                return i.ToString();
            }
        }
    }
}
