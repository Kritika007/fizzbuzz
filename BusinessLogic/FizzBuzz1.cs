﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class FizzBuzz1 : IDFizzBuzz
    {
        private static readonly DateTime _now = DateTime.UtcNow;
        public string GetFizzBuzzValue(int i)
        {
            if (_now.DayOfWeek == DayOfWeek.Wednesday)
                return "WizzWuzz";
            else
                return "FizzBuzz";
        }
    }
}
            /*f (i % 3 == 0 && i % 5 == 0)
            {

                if (_now.DayOfWeek == DayOfWeek.Wednesday)
                    return "WizzWuzz";
                else
                    return "FizzBuzz";
            }
            else
            {
                return i.ToString();
            }
        }
    }
}*/
