﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class FizzBuzz2 : IDFizzBuzz
    {
        private readonly DateTime _now = DateTime.UtcNow;
        public string GetFizzBuzzValue(int i)
        {
            if (i % 3 == 0)
            {
                if (_now.DayOfWeek == DayOfWeek.Wednesday)
                    return "Wizz";
                else
                    return " Fizz";
            }
            else
            {
                return i.ToString();
            }
        }
    }
}

