﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace MyFizzBuzzTest
{
    [TestClass]
    public class FizzBuzzTest
    {
        [TestMethod]
        public void Test_FizzBuzz()
        {
            //Arrange
            var expected = "FizzBuzz";

            //Act
            var actual = FizzBuzz(15);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        private object FizzBuzz(int p)
        {
            
            if (p % 3 == 0 && p % 5 == 0)
                return "FizzBuzz";
            if (p % 3 == 0)
                return "Fizz";
            if (p % 5 == 0)
                return "Buzz";

         return p;
        }
    }
}
